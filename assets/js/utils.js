var classes = {};
var o = {
	newClass: function(name, constructor, definition){
	    var classDefinition;
	    if(constructor === false){
	        classDefinition = function(){};
	    }else{
	        classDefinition = constructor;            
	    }
	    
	    classDefinition.prototype = definition;
	    
	    return classes[name] = classDefinition;
	},
    
	extend: function(baseClass, subClass) {
		function f() { }
		f.prototype = baseClass.prototype;
		subClass.prototype = new inheritance();
		subClass.prototype.constructor = subClass;
		subClass.baseConstructor = baseClass;
		subClass.superClass = baseClass.prototype;
        return subClass;
	}
 };