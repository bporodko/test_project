var APP;
var CONST = {
	boardRef: '.js-board',
	boardTpl: '#board-template',
	
	coinTpl: '#coin-template',
	turnIndicator: '.js-turn-indicator',
	
	turn: {
		first: 0,
		players: ['x--user-one', 'x--user-two']
	},
	
	inRow: 4,
	rowNum: 6,
	columnNum: 7,
};
$( document ).ready(function() {APP = new classes.App();});


o.newClass('App', function(argument){this.init(argument)}, {
	_board: null,
	_currentTurn: CONST.turn.first,
	
	init: function(argument){
		this._createBoard();
		
		//dom events
		$('.js-menu').on('click', _.bind(this._clickNewHandler, this));
		
		//object events
		$(this).on('change:turn', _.bind(this._turnChangeHandler, this));
	},
	
	destroy: function(){},
	
	_createBoard: function(){
		this._board = new classes.Board();
		$(this._board).trigger('change:coin', CONST.turn.players[this._currentTurn]);
	},
	
	_resetGame: function(){
		this._board.destroy();
		this._board = undefined;
		this._createBoard();
	},
	
	_changeIndicator: function(){
		var $ind = $(CONST.turnIndicator);
		$ind.html($(CONST.coinTpl).html());
		$ind.find('.x-coin').addClass(CONST.turn.players[this._currentTurn]);
	},
	
	_clickNewHandler: function(){
		this._resetGame();
	},
	
	_turnChangeHandler: function(){
		var prevTurn =  this._currentTurn;
		this._currentTurn = this._currentTurn === 0 ? 1 : 0;
		$(this._board).trigger('change:coin', CONST.turn.players[this._currentTurn]);
		
		this._changeIndicator();
	}
});


o.newClass('Board', function(argument){this.init(argument)}, {
	_$board: null,
	_currentCoin: null,
	_domCache: [],
	
	init: function(argument){
		$(CONST.boardRef).html($(CONST.boardTpl).html());
		this._$board = $(CONST.boardRef);
		
		//combines diagonals, rows and columns together 	
		this._domCache = this._grabDomDiagonals(false)
		.concat(this._grabDomDiagonals(true))
		.concat(this._grabDomRowsColumns('row'))
		.concat(this._grabDomRowsColumns('column'));
		
		//dom events
		this._$board.on('click', '.x-column', _.bind(this._clickHandler, this));
		
		//object events
		$(this).on('change:coin', _.bind(this._coinChangeHandler, this));
	},

	destroy: function(){
		this._domCache = null;
		this._$board.off('click', '.x-column');
		$(this).off('change:coin');
		
		$(CONST.boardRef).html('');
	},

	//grab rows and columns from dom
	_grabDomRowsColumns: function(dataName){
		var cellCol,
		linesCol = [],
		itemsNum = CONST.rowNum;
		
		if(dataName === 'column'){
			itemsNum = CONST.columnNum;
		}
		
		for(var i=0; i<itemsNum; i++){	
			cellCol = $('*[data-' + dataName + '="' + i + '"]').toArray();
			linesCol.push(cellCol);
		}
		
		return linesCol;
	},

	//grab diagonals from dom
	_grabDomDiagonals: function(decreasing){
		var cellCol,
		rowNum = 0;
		diagonals = [];
		
		if(decreasing){
			rowNum = CONST.rowNum - 1;
		}
		
		cellCol = _.union(this._$board.find('*[data-row="' + rowNum + '"]').toArray(), this._$board.find('*[data-column="0"]').toArray());
		
		for(var i=0; i<cellCol.length; i++){
			var cell,
			xInd = 1,
			yInd = 1,
			tempDiagonal = [],
			columnPos = $(cellCol[i]).data('column'),
			rowPos = $(cellCol[i]).data('row');
			
			if(decreasing){
				xInd = -1;
				yInd = 1;
			}
			
			tempDiagonal.push($(cellCol[i]));
			
			for(var ii=0; ii==ii; ii++){
				cell = this._$board.find('*[data-row="' + (rowPos + ii*xInd + 1*xInd) + '"][data-column="' + (columnPos + ii*yInd + 1*yInd) + '"]');
				
				if(cell.length === 0){
					break;
				}
				tempDiagonal.push(cell);
			}
			
			if(tempDiagonal.length >= CONST.inRow){
				diagonals.push(tempDiagonal);
			}
		}
		
		return diagonals;
	},

	_lockColumn: function(column){
		$(column).closest('.x-column').data('locked', 'true');
	},
	
	_winCheck: function(){
		for(var i=0; i<=this._domCache.length; i++){	
			this._checkWinChain(this._domCache[i]);
		}
	},
	
	_checkWinChain: function(cellCol){
		var row = 0;
		
		for (var cellNum in cellCol) {
			var $cell = $(cellCol[cellNum]);
			
			if($cell.has('.x-coin').length && $cell.find('.x-coin').hasClass(this._currentCoin))
			{
				row++;
			}else{
				row = 0;
			}
			
			if(row >= 4){
				alert('You win!'); 
				return; 
			}
		}
	},
	
	_coinChangeHandler: function(event, data){
		this._currentCoin = data;
	},
	
	_clickHandler: function(event){
		var cellCol, $column = $(event.currentTarget),
		stopLoop = false;
		
		if($column.data('locked')){ return;}
		
		cellCol = $column.find('.x-cell')
		
		_.each(cellCol.toArray().reverse(), function(cell){
			if(!stopLoop){
				$cell = $(cell);
				
				//checks is cell filled goes from bottom to top of rows
				if(!$cell.has('.x-coin').length){
					$cell.html($(CONST.coinTpl).html());
					$cell.find('.x-coin').addClass(this._currentCoin);
					
					this._winCheck();
					$(APP).trigger('change:turn');
					
					//locks colomn when each cell filled
					if($cell.data('row') === 0){
						this._lockColumn($column);
					}
					stopLoop = true;
				}
			}
		}, this);
	},
	
});

